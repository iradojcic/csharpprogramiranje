﻿using System;
namespace McOrders
{
    public class Drink : Ingestible
    {
        public Drink(int orderId) : base(orderId, "Drink", 2){}
    }
}
