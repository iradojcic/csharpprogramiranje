namespace McOrders
{
    public partial class FormMain : Form
    {
        Orders listOfOrders;
        Iteration orderCounter;
        public FormMain()
        {
            InitializeComponent();
            listOfOrders = new Orders();
            orderCounter = new Iteration();
            listOfOrders.modifyForm += refreshDelegator;
        }
    }
}