﻿using System;
namespace McOrders
{
    public class Iteration
    {
        public int CurrentId {  get; set; }
        public Iteration()
        {
            this.CurrentId = 1;
        }
        public void Advance()
        {
            CurrentId++;
        }
    }
}

