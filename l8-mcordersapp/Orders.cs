﻿using System;
namespace McOrders
{
    public class Orders
    {
        public delegate void OrderShifted();
        public event OrderShifted modifyForm;
        public List<Ingestible> _prepping { get; set; }
        public List<Ingestible> PREPARED_ { get; set; }
        public Orders()
        {
            this._prepping = new List<Ingestible>();
            this.PREPARED_ = new List<Ingestible>();
        }

        public void AddToPrepping(Ingestible ingestible)
        {
            Task delay_prepping = new Task(() => {
                {
                    Thread.Sleep(2000);
                    _prepping.Add(ingestible);
                    modifyForm?.Invoke();
                    Thread.Sleep(ingestible.PrepTime * 1000);
                    _prepping.Remove(ingestible);
                    PREPARED_.Add(ingestible);
                    modifyForm?.Invoke();
                }
            });
            delay_prepping.Start();
        }
    }
}


