﻿using System;
namespace McOrders
{
public class Ingestible
{
    public int OrderId { get; set; }
	public string Name { get; set; }
	public int PrepTime { get; set; }
	public Ingestible()
	{
		this.Name = "";
		this.PrepTime = 0;
	}

	public Ingestible(int orderId, string name, int prepTime)
    {
		this.OrderId = orderId;
        this.Name = name;
        this.PrepTime = prepTime;
    }
    public override string ToString()
    {
            return (this.OrderId).ToString("000") + " (" + this.Name + ")";
    }
 }
}
