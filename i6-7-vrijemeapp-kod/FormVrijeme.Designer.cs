﻿namespace VrijemeApp
{
    partial class FormVrijeme
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            cbGrad = new ComboBox();
            lblGrad = new Label();
            lblTemperatura = new Label();
            lblVlaga = new Label();
            lblTlak = new Label();
            scKontejner = new SplitContainer();
            lbNajtopliji = new ListBox();
            lblNajtopliji = new Label();
            lbNajhladniji = new ListBox();
            lblNajhladniji = new Label();
            lblRefresh = new Label();
            ((System.ComponentModel.ISupportInitialize)scKontejner).BeginInit();
            scKontejner.Panel1.SuspendLayout();
            scKontejner.Panel2.SuspendLayout();
            scKontejner.SuspendLayout();
            SuspendLayout();
            // 
            // cbGrad
            // 
            cbGrad.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            cbGrad.FormattingEnabled = true;
            cbGrad.Location = new Point(12, 12);
            cbGrad.Name = "cbGrad";
            cbGrad.Size = new Size(497, 23);
            cbGrad.TabIndex = 0;
            cbGrad.Text = "Odaberite grad";
            cbGrad.SelectedIndexChanged += cbGrad_SelectedIndexChanged;
            // 
            // lblGrad
            // 
            lblGrad.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblGrad.Font = new Font("Segoe UI", 15.75F, FontStyle.Bold, GraphicsUnit.Point);
            lblGrad.Location = new Point(12, 38);
            lblGrad.Name = "lblGrad";
            lblGrad.Size = new Size(497, 44);
            lblGrad.TabIndex = 1;
            lblGrad.Text = "(grad nije odabran)";
            lblGrad.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblTemperatura
            // 
            lblTemperatura.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTemperatura.Font = new Font("Segoe UI", 15.75F, FontStyle.Bold, GraphicsUnit.Point);
            lblTemperatura.Location = new Point(12, 82);
            lblTemperatura.Name = "lblTemperatura";
            lblTemperatura.Size = new Size(497, 44);
            lblTemperatura.TabIndex = 2;
            lblTemperatura.Text = "? °C";
            lblTemperatura.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblVlaga
            // 
            lblVlaga.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblVlaga.Font = new Font("Segoe UI", 15.75F, FontStyle.Bold, GraphicsUnit.Point);
            lblVlaga.Location = new Point(12, 126);
            lblVlaga.Name = "lblVlaga";
            lblVlaga.Size = new Size(497, 44);
            lblVlaga.TabIndex = 3;
            lblVlaga.Text = "? %";
            lblVlaga.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblTlak
            // 
            lblTlak.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTlak.Font = new Font("Segoe UI", 15.75F, FontStyle.Bold, GraphicsUnit.Point);
            lblTlak.Location = new Point(12, 170);
            lblTlak.Name = "lblTlak";
            lblTlak.Size = new Size(497, 44);
            lblTlak.TabIndex = 4;
            lblTlak.Text = "? hPa";
            lblTlak.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // scKontejner
            // 
            scKontejner.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            scKontejner.Location = new Point(12, 260);
            scKontejner.Name = "scKontejner";
            // 
            // scKontejner.Panel1
            // 
            scKontejner.Panel1.Controls.Add(lbNajtopliji);
            scKontejner.Panel1.Controls.Add(lblNajtopliji);
            // 
            // scKontejner.Panel2
            // 
            scKontejner.Panel2.Controls.Add(lbNajhladniji);
            scKontejner.Panel2.Controls.Add(lblNajhladniji);
            scKontejner.Size = new Size(497, 292);
            scKontejner.SplitterDistance = 247;
            scKontejner.TabIndex = 5;
            // 
            // lbNajtopliji
            // 
            lbNajtopliji.Dock = DockStyle.Fill;
            lbNajtopliji.FormattingEnabled = true;
            lbNajtopliji.ItemHeight = 15;
            lbNajtopliji.Location = new Point(0, 23);
            lbNajtopliji.Name = "lbNajtopliji";
            lbNajtopliji.Size = new Size(247, 269);
            lbNajtopliji.TabIndex = 1;
            // 
            // lblNajtopliji
            // 
            lblNajtopliji.Dock = DockStyle.Top;
            lblNajtopliji.Location = new Point(0, 0);
            lblNajtopliji.Name = "lblNajtopliji";
            lblNajtopliji.Size = new Size(247, 23);
            lblNajtopliji.TabIndex = 0;
            lblNajtopliji.Text = "Najtopliji gradovi";
            lblNajtopliji.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lbNajhladniji
            // 
            lbNajhladniji.Dock = DockStyle.Fill;
            lbNajhladniji.FormattingEnabled = true;
            lbNajhladniji.ItemHeight = 15;
            lbNajhladniji.Location = new Point(0, 23);
            lbNajhladniji.Name = "lbNajhladniji";
            lbNajhladniji.Size = new Size(246, 269);
            lbNajhladniji.TabIndex = 1;
            // 
            // lblNajhladniji
            // 
            lblNajhladniji.Dock = DockStyle.Top;
            lblNajhladniji.Location = new Point(0, 0);
            lblNajhladniji.Name = "lblNajhladniji";
            lblNajhladniji.Size = new Size(246, 23);
            lblNajhladniji.TabIndex = 0;
            lblNajhladniji.Text = "Najhladniji gradovi";
            lblNajhladniji.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblRefresh
            // 
            lblRefresh.AutoSize = true;
            lblRefresh.Location = new Point(165, 229);
            lblRefresh.Name = "lblRefresh";
            lblRefresh.Size = new Size(59, 15);
            lblRefresh.TabIndex = 6;
            lblRefresh.Text = "lblRefresh";
            // 
            // FormVrijeme
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(521, 564);
            Controls.Add(lblRefresh);
            Controls.Add(scKontejner);
            Controls.Add(lblTlak);
            Controls.Add(lblVlaga);
            Controls.Add(lblTemperatura);
            Controls.Add(lblGrad);
            Controls.Add(cbGrad);
            Name = "FormVrijeme";
            Text = "VrijemeApp";
            scKontejner.Panel1.ResumeLayout(false);
            scKontejner.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)scKontejner).EndInit();
            scKontejner.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cbGrad;
        private Label lblGrad;
        private Label lblTemperatura;
        private Label lblVlaga;
        private Label lblTlak;
        private SplitContainer scKontejner;
        private Label lblNajtopliji;
        private Label lblNajhladniji;
        private ListBox lbNajtopliji;
        private ListBox lbNajhladniji;
        private Label lblRefresh;
    }
}