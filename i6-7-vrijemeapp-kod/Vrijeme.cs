using System.Xml;
using VrijemeApp;
using System;
using System.Threading.Tasks;
using System.Threading;
using static VrijemeApp.FormVrijeme;
using System.Diagnostics;

class Vrijeme {
    private List<VrijemePodatak> podaci;
    private XmlDocument vrijeme;
    public delegate void VrijemeDelegate(Object sender, VrijemeArgs e);
    public event VrijemeDelegate PodaciOsvjezeni;
    public event VrijemeDelegate GreskaPrilikomOsvjezavanja;

    public Vrijeme() {
        try
        {
            vrijeme = new XmlDocument();
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");
            PodaciOsvjezeni?.Invoke(this, new VrijemeArgs());
            Task osvjezavanje = new Task(() => {
                while (true)
                {
                    DohvatiPodatke();
                    Thread.Sleep(15000);
                }
            });
            osvjezavanje.Start();

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            GreskaPrilikomOsvjezavanja?.Invoke(this, new VrijemeArgs(e.Message));
        }
    }

    public List<VrijemePodatak> DohvatiPodatke() {
        podaci = new List<VrijemePodatak>();

        XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
        foreach (XmlNode grad in gradovi) {
            podaci.Add(new VrijemePodatak(
                grad["GradIme"].InnerText,
                grad["Podatci"]["Temp"].InnerText,
                grad["Podatci"]["Vlaga"].InnerText,
                grad["Podatci"]["Tlak"].InnerText
            ));
        }
        podaci.Sort(new VrijemePodatakComparer());
        return podaci;
    }

    public IEnumerable<object> DohvatiNajtoplije(int amount)
    {
        var query = podaci
        .OrderByDescending(grad => grad.Temperatura)
        .Take(amount)
        .Select(grad => grad);

        return query;
    }
    public IEnumerable<object> DohvatiNajhladnije(int amount)
    {
        var query = podaci
        .OrderBy(grad => grad.Temperatura)
        .Take(amount)
        .Select(grad => grad);

        return query;
    }
}