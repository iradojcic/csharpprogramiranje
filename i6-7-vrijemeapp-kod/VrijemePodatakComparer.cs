﻿using System;
namespace VrijemeApp;
public class VrijemePodatakComparer : IComparer<VrijemePodatak>
{
	public int Compare(VrijemePodatak? A, VrijemePodatak? B)
	{
		return A.Grad.CompareTo(B.Grad);
	}
}
