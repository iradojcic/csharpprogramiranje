﻿
using System;
using System.Threading.Tasks;
using System.Threading;
using static VrijemeApp.FormVrijeme;
using System.Diagnostics;

namespace VrijemeApp
{
    public partial class FormVrijeme : Form
    {
        private Vrijeme Gradovi;
        public FormVrijeme()
        {
            InitializeComponent();

            Gradovi = new Vrijeme();
            Gradovi.PodaciOsvjezeni += PodaciUspjesnoOsvjezeni;
            Gradovi.GreskaPrilikomOsvjezavanja += PodaciNeuspjesnoOsvjezeni;
            OsvjeziPodatke();
            OsvjeziPopisGradova();
        }

        private void OsvjeziPodatke()
        {
            Gradovi.DohvatiPodatke();
        }
        private void OsvjeziPopisGradova()
        {
            OsvjeziDisplay(new VrijemeArgs());
        }

        private void PodaciUspjesnoOsvjezeni(object sender, VrijemeArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {OsvjeziDisplay(e);}));
            }
            else {OsvjeziDisplay(e);}
        }

        private void PodaciNeuspjesnoOsvjezeni(object sender, VrijemeArgs e)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate {FailedRefresh(e);}));
            }
            else {FailedRefresh(e);}
        }

        /*
        private void OsvjeziPrikazZadnjegRefresha(VrijemeArgs e)
        {
            lblRefresh.Text = "Zadnje osvježavanje: " + e.NajnovijeVrijeme;
        }
        */
        private void OsvjeziPrikazZadnjegRefresha(VrijemeArgs e)
        {
            if (lblRefresh.InvokeRequired)
            {
                lblRefresh.Invoke(new MethodInvoker(delegate { OsvjeziPrikazZadnjegRefresha(e); }));
                return;
            }
            lblRefresh.Text = "Zadnje osvježavanje: " + DateTime.Now.ToString();
        }


        private void OsvjeziDisplay(VrijemeArgs e)
        {
            var trenutniCbGrad = cbGrad;
            try
            {
                cbGrad.Items.Clear();
                cbGrad.Items.AddRange(Gradovi.DohvatiPodatke().ToArray());
            }
            catch
            {
                cbGrad = trenutniCbGrad;
            }
            VrijemePodatak item;
            try
            {
                item = (VrijemePodatak)cbGrad.Items[cbGrad.SelectedIndex];
                PrikaziPodatkeZaGrad(item);
            }
            catch
            {
            }
            OsvjeziPrikazZadnjegRefresha(e);
            OsvjeziNajhladnijeINajtoplijeGradove();
        }

        private void FailedRefresh(VrijemeArgs e)
        {
            lblRefresh.Text = "Podaci nisu uspješno osvježeni";
        }

        private void PrikaziPodatkeZaGrad(VrijemePodatak grad)
        {
            if (grad == null)
            {
                grad = new VrijemePodatak("Nije uspješno dohvaćeno", "Nije uspješno dohvaćeno", "Nije uspješno dohvaćeno", "Nije uspješno dohvaćeno");
            }
            lblGrad.Text = grad.Grad;
            lblTemperatura.Text = grad.GetTemperatura();
            lblVlaga.Text = grad.GetVlaga();
            lblTlak.Text = grad.GetTlak();
        }

        private void OsvjeziNajhladnijeINajtoplijeGradove()
        {
            var trenutniNajtopliji = lbNajtopliji;
            var trenutniNajhladniji = lbNajhladniji;
            try
            {
                var najtopliji = Gradovi.DohvatiNajtoplije(5).ToList();
                var najhladniji = Gradovi.DohvatiNajhladnije(5).ToList();
                lbNajtopliji.DataSource = najtopliji;
                lbNajhladniji.DataSource = najhladniji;
            }
            catch
            {
                lbNajtopliji = trenutniNajtopliji;
                lbNajhladniji = trenutniNajhladniji;
            }
        }

        private void cbGrad_SelectedIndexChanged(object sender, EventArgs e)
        {
            VrijemePodatak item;
            try
            {
                item = (VrijemePodatak)cbGrad.Items[cbGrad.SelectedIndex];
                PrikaziPodatkeZaGrad(item);
            }
            catch
            {

            }
        }
    }
}