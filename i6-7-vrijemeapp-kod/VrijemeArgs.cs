﻿using System;
namespace VrijemeApp;
public class VrijemeArgs : EventArgs
{
	public DateTime NajnovijeVrijeme {get; set;}
    public string Msg { get; set; }
    public VrijemeArgs(string msg = "")
	{
        NajnovijeVrijeme = DateTime.Now;
        Msg = msg;
	}

    public override string ToString()
    {
        return "Zadnje osvježavanje: " + NajnovijeVrijeme.ToString();
    }
}
